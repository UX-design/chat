var gyro = new function () {

  	var me = this ;

   	me.orientation = window.orientation ; 
   	me.coord = {} ;
   	me.round = function ( number, decimals ) {
        if ( decimals > 0 )
            var x = Math.pow( 10, decimals ) ;
        else 
            var x = 1 ; return Math.round( number * x ) / x ;
    }

    me.refresh = function ( e ) {
        if ( e ) {
            if ( me.orientation == 0 ) {
                me.coord.ga = me.round( e.alpha, 3 )
               ,me.coord.gb = me.round( e.beta, 3 )
               ,me.coord.gg = me.round( e.gamma , 3 ) ;
            }
            if ( me.orientation == 90 ) {
                me.coord.ga = me.round( e.alpha, 3 )
               ,me.coord.gg = me.round( e.beta, 3 )
               ,me.coord.gb = me.round( e.gamma , 3 ) ;
            }
            if ( me.orientation == -90 ) {
                me.coord.ga = me.round( e.alpha, 3 )
               ,me.coord.gg = me.round( e.beta * -1, 3 )
               ,me.coord.gb = me.round( e.gamma , 3 ) ;
            }
        } else {
            me.coord.ga = 0
           ,me.coord.gb = 0
           ,me.coord.gg = 0 ;
        }
        if( me.coord.gg == undefined ){
            gyro = undefined ;
        };
    }

    window.addEventListener( 'orientationchange', function() {
        me.orientation = window.orientation ; 
    }, false ) ;

    window.addEventListener( 'deviceorientation', me.refresh, false ) ;

    
}


