var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);


app.get('/', function(req, res){
	/* INVIA AL CLIENT QUESTO TESTO 
  res.send('<h1>Hello world</h1>');*/
  	/* INVIA UN FILE AL CLIENT */
  res.sendFile("mobile.html", {"root": __dirname});
});



io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    io.emit('chat message', msg);
  });
});

io.on('connection', function(socket){
  socket.on('pos', function(msg){
    io.emit('pos', msg);
  });
});

io.on('connection', function(socket){
  socket.broadcast.emit('hi');
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});